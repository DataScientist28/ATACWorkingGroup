RStudio Installation
================
Alex Chitsazan
February 1, 2017

-   [Overview](#overview)
-   [Download R](#download-r)
-   [Download RStudio](#download-rstudio)
-   [Test to make sure it works](#test-to-make-sure-it-works)
-   [Session 1 - Intro to R and Genomic Ranges](#session-1---intro-to-r-and-genomic-ranges)

Overview
========

Please do the following things before the first ATAC-seq group meeting. This will allow us to spend most of our time on the actual code rather than installation.

Download R
==========

First downlaod R. You have to download the scripting language for RStudio to recognize the code. It can be downloaded [here](https://ftp.osuosl.org/pub/cran/).

Download RStudio
================

The second thing that needs to be downloaded is RStudio. RStudio is a GUI (Graphical User Interface) that allows you to run R code. More explanation will occur at the meeting. It can be downloaded [here](https://www.rstudio.com/products/rstudio/download/). Download the installer build for easy installation.

Test to make sure it works
==========================

Then open RStudio and type `plot(1)` into the console line. Your screen should end up with a plot like this in the lower right hand corner. ![RStudio](RStudioTest.png)

Session 1 - Intro to R and Genomic Ranges
=========================================

This folder should contain the source code for the tutorial. Please when you get a change download the material provided. Download everything but the README.md

Feel free to email me if you have any questions!

Alex Chitsazan
