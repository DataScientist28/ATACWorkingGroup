---
title: "ATAC-seq Working Group Sesession 2- Advanced GenomicRanges"
author: "Alex Chitsazan\\"
date: "February 15, 2017"
always_allow_html: yes  
output: 
  github_document:
    toc: true
    toc_depth: 4
---

## Setting up Environment and Refresher On Prev Week Tutuorial
```{r setup, include=FALSE}
knitr::opts_knit$set(root.dir = "~/Desktop/ATACWorkingGroupGitLab//WorkingDirectory/")
```


```{r barplotChroms, cache=TRUE}
library(GenomicRanges)

##### setwd("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/")
##### Funciton to read in the GRanges objects
Peaks2Ranges <- function(PEAKFILE) {
  ###### Load the MACS results of peak locations and associated metrics
  peaks.table <- read.table(PEAKFILE, 
                           header = FALSE, 
                           sep = "\t")
  
  ###### Add Appropriate Column Names
  colnames(peaks.table) <- c("Chrom", "Start", "End", "Name", "Score", "Strand", 
                            "FoldChange", "pValue", "qValue", "RelativeSummitPosition")
  ###### Change Strand from "." to "*" because that's how GRanges likes it
  peaks.table$Strand <- "*"
  
  ###### Convert to GRanges Format 
  peak.gr <- with(peaks.table, GRanges(Chrom, 
                               IRanges(Start, End),
                               Strand, 
                               Name, 
                               FoldChange, 
                               qValue, 
                               pValue,
                               Score,
                               RelativeSummitPosition))
  return(peak.gr)
}

###### Running Function
A250.gr <- Peaks2Ranges("A250_Wills_S1_001_rmMT_peaks.narrowPeak")
B250.gr <- Peaks2Ranges("B250_Wills_S2_001_rmMT_peaks.narrowPeak")


##### Number of Peaks per chromosome
chroms <- table(seqnames(B250.gr))[1:38]

##### Sort 
chroms <- chroms[order(as.numeric(names(chroms)))]

##### barplot of number of peaks per chromosome
barplot(chroms,
        col = "purple",
        xlab = "Chromosome",
        ylab = "Number of Peaks",
        main = "Number of Peaks Per Chromosome")
```


## Example of Finding overlaps per sample

```{r Overlaps, cache=TRUE, warning=FALSE}
##### Find the overlapping regions between Sample A250 and B250 
overlaps <- findOverlaps(A250.gr, B250.gr)
head(overlaps)
A250.gr[8]
B250.gr[11]

##### Convert to dataframe
overlap.df <- data.frame(overlaps) 

##### How many of A250 overlap with B250
length(unique(overlap.df$queryHits))
length(A250.gr)

##### How many B250 overlap with A250
length(unique(overlap.df$subjectHits))
length(B250.gr)
```

## Example Distance to TSS 

```{r DistanceToTSS, cache=TRUE, warning=FALSE}
library(GenomicFeatures)
library(biomaRt)
library(GenomicAlignments)

# ##### showing how to look to get gene annotation
# listMarts()
# ensembl <- useMart("ENSEMBL_MART_ENSEMBL")
# listDatasets(ensembl)
# # CanFam.txdb <- makeTxDbFromBiomart(biomart = "ENSEMBL_MART_ENSEMBL",
# #                                    dataset = "cfamiliaris_gene_ensembl",
# #                                    host = "ensembl.org")
# 
# ##### get a list of transcripts associated with each gene
# tx_by_gene <- transcriptsBy(CanFam.txdb, 'gene')
# 
# ##### reduce transcripts on each locus
# genic.tx <- reduce(tx_by_gene)
# 
# ##### convert from GRangesList to Granges
# genes.gr <- unlist(genic.tx)
# genes.gr
# ##### saveRDS(genes.gr, "CanFam3_1.gr")

##### Load in preloaded Dog Genome
genes.gr <- readRDS(file = "CanFam3_1.gr")

##### Reshape to one single point of the TSS
TSS.gr       <- promoters(genes.gr, upstream=0, downstream = 1)


##### Run DistanceToNearest (GRanges fn) to get distances from TSS for all peaks
TSS.distance <- data.frame(distanceToNearest(A250.gr, TSS.gr))

##### Name the columns               
colnames(TSS.distance) <- c("Peaks", "Genes", "Distance")

##### Explore the Data
head(TSS.distance, 20)
A250.gr[14]
TSS.gr[17594]

##### Add column to GRanges
A250.gr$Gene <- NA
A250.gr$Gene[TSS.distance$Peaks] <- names(TSS.gr)[TSS.distance$Genes]
A250.gr$DistanceToTSS <- NA
A250.gr$DistanceToTSS[TSS.distance$Peaks] <- TSS.distance$Distance
A250.gr
```


## Coverage Views

```{r CoverageViews, cache=TRUE, warning=FALSE}
##### Basic setup to tell comp where the bam file is
bam    <- BamFile("A250_subsample.bam")

##### Read in the bamfile
aln    <- readGAlignments(bam)
aln    <- GRanges(aln)

##### Fucntion that shifts reads based on strand aligned to
shiftbasedonstrand <- function(alnGR, shiftsize) {
    pos <- which(strand(alnGR) == "+")
    neg <- which(strand(alnGR) == "-")
    shifts <- vector(length = length(alnGR))
    shifts[pos] <- shiftsize
    shifts[neg] <- -shiftsize
    shiftedGR <- shift(alnGR, shifts)
    return(shiftedGR)
}
aln <- shiftbasedonstrand(aln, -100)

##### Resize reads to fixed fragment length (200)
aln <- resize(aln, 200)


##### Extract basepair coverage
cov <- coverage(aln)


##### Convert to RPM
rpmfun <- function(covob, libsize) {
  signif(covob/libsize * 10^6, 5)
}
nreads <- length(aln)
rpmfile <- mapply(rpmfun,
                  covob = cov,
                  libsize=nreads)
rpmfile <- as(rpmfile, "SimpleRleList")
rpmfile
## saveRDS(rpmfile, "Test.RPM")
## export.bw(object=rpmfile, con=bw_output)



```

```{r Views, cache=TRUE, warning=FALSE}
##### Read in full coverage RDS
A250.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/A250_rpm.RDS")

##### Find the converage at each individual basepair for each gene
Promoters.gr <- promoters(genes.gr, upstream=2000, downstream = 2000)

##### Looking at specific Ranges
Promoters.gr["ENSCAFG00000031236"]

##### Get the chromosome name
ch <- as.character(unique(seqnames(Promoters.gr["ENSCAFG00000031236"])))

##### Pull out vector of cooverage at that area
view <- Views(A250.rpm[ch][[1]], ranges(Promoters.gr["ENSCAFG00000031236"]))

##### Convert to numeric non-rle style
cov.v <- as.numeric(view[1][[1]])
if (as.logical(strand(Promoters.gr["ENSCAFG00000031236"]) == "-")) {
  cov <- rev(cov)
}

##### Plot what looks like 
plot(cov.v, type = "l")
plot(cov.v,
     main = "Coverage at ENSCAFG00000031236",
     xaxt = "n",
     type = "l",
     col  = "red",
     ylab = "Reads Per Million",
     xlab = "Position")
axis(1, at=c(0, 2000, 4000), labels=c("-2000", "TSS", "2000"))



######## Lets plot different samples
##### Make a function
ExtractCov <- function(gr, rpmcov, gene) {
  ch <- as.character(unique(seqnames(gr[gene])))
  view <- Views(rpmcov[ch][[1]], ranges(gr[gene]))
  cov.v <- as.numeric(view[1][[1]])
  if (as.logical(strand(gr[gene]) == "-")) {
    cov.v <- rev(cov.v)
  }
  return(cov.v)
}



##### No let's see how different samples compare
B250.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/B250_rpm.RDS")
C250.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/C250_rpm.RDS")
C50.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/C50_rpm.RDS")


##### Extract Coverage
B250.cov <- ExtractCov(Promoters.gr, B250.rpm, "ENSCAFG00000031236")
C250.cov <- ExtractCov(Promoters.gr, C250.rpm, "ENSCAFG00000031236")
C50.cov  <- ExtractCov(Promoters.gr, C50.rpm, "ENSCAFG00000031236")

##### plot the vector 
plot(cov.v,
     main = "Coverage at ENSCAFG00000031236",
     xaxt = "n",
     type = "l",
     col  = "red",
     ylab = "Reads Per Million",
     xlab = "Position")
##### Plot lines for other samples
lines(B250.cov, col = "blue")
lines(C250.cov, col = "purple")
lines(C50.cov,  col = "green")
##### set the Axis
axis(1, at=c(0, 2000, 4000), labels=c("-2000", "TSS", "2000"))
legend(3000,3, # places a legend at the appropriate place 
       c("A250","B250", "C250", "C50"), # puts text in the legend 
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),
       col=c("red","blue", "purple", "green"))

```


