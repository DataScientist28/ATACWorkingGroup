ATAC-seq Working Group Sesession 2- Advanced GenomicRanges
================
Alex Chitsazan
February 15, 2017

-   [Setting up Environment and Refresher On Prev Week Tutuorial](#setting-up-environment-and-refresher-on-prev-week-tutuorial)
-   [Example of Finding overlaps per sample](#example-of-finding-overlaps-per-sample)
-   [Example Distance to TSS](#example-distance-to-tss)
-   [Coverage Views](#coverage-views)

Setting up Environment and Refresher On Prev Week Tutuorial
-----------------------------------------------------------

``` r
library(GenomicRanges)

##### setwd("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/")
##### Funciton to read in the GRanges objects
Peaks2Ranges <- function(PEAKFILE) {
  ###### Load the MACS results of peak locations and associated metrics
  peaks.table <- read.table(PEAKFILE, 
                           header = FALSE, 
                           sep = "\t")
  
  ###### Add Appropriate Column Names
  colnames(peaks.table) <- c("Chrom", "Start", "End", "Name", "Score", "Strand", 
                            "FoldChange", "pValue", "qValue", "RelativeSummitPosition")
  ###### Change Strand from "." to "*" because that's how GRanges likes it
  peaks.table$Strand <- "*"
  
  ###### Convert to GRanges Format 
  peak.gr <- with(peaks.table, GRanges(Chrom, 
                               IRanges(Start, End),
                               Strand, 
                               Name, 
                               FoldChange, 
                               qValue, 
                               pValue,
                               Score,
                               RelativeSummitPosition))
  return(peak.gr)
}

###### Running Function
A250.gr <- Peaks2Ranges("A250_Wills_S1_001_rmMT_peaks.narrowPeak")
B250.gr <- Peaks2Ranges("B250_Wills_S2_001_rmMT_peaks.narrowPeak")


##### Number of Peaks per chromosome
chroms <- table(seqnames(B250.gr))[1:38]

##### Sort 
chroms <- chroms[order(as.numeric(names(chroms)))]

##### barplot of number of peaks per chromosome
barplot(chroms,
        col = "purple",
        xlab = "Chromosome",
        ylab = "Number of Peaks",
        main = "Number of Peaks Per Chromosome")
```

![](README_files/figure-markdown_github/barplotChroms-1.png)

Example of Finding overlaps per sample
--------------------------------------

``` r
##### Find the overlapping regions between Sample A250 and B250 
overlaps <- findOverlaps(A250.gr, B250.gr)
head(overlaps)
```

    ## Hits object with 6 hits and 0 metadata columns:
    ##       queryHits subjectHits
    ##       <integer>   <integer>
    ##   [1]         2           1
    ##   [2]         3           2
    ##   [3]         4           3
    ##   [4]         6           5
    ##   [5]         8          11
    ##   [6]         9          13
    ##   -------
    ##   queryLength: 21372 / subjectLength: 34166

``` r
A250.gr[8]
```

    ## GRanges object with 1 range and 6 metadata columns:
    ##       seqnames           ranges strand |                          Name
    ##          <Rle>        <IRanges>  <Rle> |                      <factor>
    ##   [1]        1 [191676, 192213]      * | A250_Wills_S1_001_rmMT_peak_8
    ##       FoldChange    qValue    pValue     Score RelativeSummitPosition
    ##        <numeric> <numeric> <numeric> <integer>              <integer>
    ##   [1]    4.60123   4.80763   7.72091        48                    261
    ##   -------
    ##   seqinfo: 1889 sequences from an unspecified genome; no seqlengths

``` r
B250.gr[11]
```

    ## GRanges object with 1 range and 6 metadata columns:
    ##       seqnames           ranges strand |                           Name
    ##          <Rle>        <IRanges>  <Rle> |                       <factor>
    ##   [1]        1 [191716, 192015]      * | B250_Wills_S2_001_rmMT_peak_11
    ##       FoldChange    qValue    pValue     Score RelativeSummitPosition
    ##        <numeric> <numeric> <numeric> <integer>              <integer>
    ##   [1]    4.44444   6.13622   8.94637        61                     85
    ##   -------
    ##   seqinfo: 2197 sequences from an unspecified genome; no seqlengths

``` r
##### Convert to dataframe
overlap.df <- data.frame(overlaps) 

##### How many of A250 overlap with B250
length(unique(overlap.df$queryHits))
```

    ## [1] 15854

``` r
length(A250.gr)
```

    ## [1] 21372

``` r
##### How many B250 overlap with A250
length(unique(overlap.df$subjectHits))
```

    ## [1] 15405

``` r
length(B250.gr)
```

    ## [1] 34166

Example Distance to TSS
-----------------------

``` r
library(GenomicFeatures)
library(biomaRt)
library(GenomicAlignments)

# ##### showing how to look to get gene annotation
# listMarts()
# ensembl <- useMart("ENSEMBL_MART_ENSEMBL")
# listDatasets(ensembl)
# # CanFam.txdb <- makeTxDbFromBiomart(biomart = "ENSEMBL_MART_ENSEMBL",
# #                                    dataset = "cfamiliaris_gene_ensembl",
# #                                    host = "ensembl.org")
# 
# ##### get a list of transcripts associated with each gene
# tx_by_gene <- transcriptsBy(CanFam.txdb, 'gene')
# 
# ##### reduce transcripts on each locus
# genic.tx <- reduce(tx_by_gene)
# 
# ##### convert from GRangesList to Granges
# genes.gr <- unlist(genic.tx)
# genes.gr
# ##### saveRDS(genes.gr, "CanFam3_1.gr")

##### Load in preloaded Dog Genome
genes.gr <- readRDS(file = "CanFam3_1.gr")

##### Reshape to one single point of the TSS
TSS.gr       <- promoters(genes.gr, upstream=0, downstream = 1)


##### Run DistanceToNearest (GRanges fn) to get distances from TSS for all peaks
TSS.distance <- data.frame(distanceToNearest(A250.gr, TSS.gr))

##### Name the columns               
colnames(TSS.distance) <- c("Peaks", "Genes", "Distance")

##### Explore the Data
head(TSS.distance, 20)
```

    ##    Peaks Genes Distance
    ## 1      1 21921   262882
    ## 2      2 21921   254863
    ## 3      3 21921   249688
    ## 4      4 21921   246051
    ## 5      5 21921   240261
    ## 6      6 21921   223440
    ## 7      7 21921   203417
    ## 8      8 21921    77791
    ## 9      9 21921    69494
    ## 10    10     3     2708
    ## 11    11     4     3111
    ## 12    12     5      736
    ## 13    13     5    16670
    ## 14    14 17594        0
    ## 15    15 22946     1158
    ## 16    16     7       98
    ## 17    17     7    13619
    ## 18    18     8    23792
    ## 19    19     8       92
    ## 20    20     9       51

``` r
A250.gr[14]
```

    ## GRanges object with 1 range and 6 metadata columns:
    ##       seqnames           ranges strand |                           Name
    ##          <Rle>        <IRanges>  <Rle> |                       <factor>
    ##   [1]        1 [722003, 722239]      * | A250_Wills_S1_001_rmMT_peak_14
    ##       FoldChange    qValue    pValue     Score RelativeSummitPosition
    ##        <numeric> <numeric> <numeric> <integer>              <integer>
    ##   [1]    3.59116    2.7528   5.40567        27                    107
    ##   -------
    ##   seqinfo: 1889 sequences from an unspecified genome; no seqlengths

``` r
TSS.gr[17594]
```

    ## GRanges object with 1 range and 0 metadata columns:
    ##                      seqnames           ranges strand
    ##                         <Rle>        <IRanges>  <Rle>
    ##   ENSCAFG00000024219        1 [722173, 722173]      -
    ##   -------
    ##   seqinfo: 3268 sequences (1 circular) from an unspecified genome

``` r
##### Add column to GRanges
A250.gr$Gene <- NA
A250.gr$Gene[TSS.distance$Peaks] <- names(TSS.gr)[TSS.distance$Genes]
A250.gr$DistanceToTSS <- NA
A250.gr$DistanceToTSS[TSS.distance$Peaks] <- TSS.distance$Distance
A250.gr
```

    ## GRanges object with 21372 ranges and 8 metadata columns:
    ##           seqnames                 ranges strand |
    ##              <Rle>              <IRanges>  <Rle> |
    ##       [1]        1         [ 6810,  7122]      * |
    ##       [2]        1         [14838, 15141]      * |
    ##       [3]        1         [20004, 20316]      * |
    ##       [4]        1         [23639, 23953]      * |
    ##       [5]        1         [29543, 29743]      * |
    ##       ...      ...                    ...    ... .
    ##   [21368]        X [123528125, 123528493]      * |
    ##   [21369]        X [123717289, 123717744]      * |
    ##   [21370]        X [123841524, 123841852]      * |
    ##   [21371]        X [123865647, 123865935]      * |
    ##   [21372]        X [123867557, 123867763]      * |
    ##                                        Name FoldChange    qValue    pValue
    ##                                    <factor>  <numeric> <numeric> <numeric>
    ##       [1]     A250_Wills_S1_001_rmMT_peak_1    3.93258   5.16883   8.11156
    ##       [2]     A250_Wills_S1_001_rmMT_peak_2    3.51064   6.89208   9.96358
    ##       [3]     A250_Wills_S1_001_rmMT_peak_3    4.80769  16.26406  19.82099
    ##       [4]     A250_Wills_S1_001_rmMT_peak_4    3.63881   5.90736   8.90987
    ##       [5]     A250_Wills_S1_001_rmMT_peak_5    4.65116   4.08593   6.92693
    ##       ...                               ...        ...       ...       ...
    ##   [21368] A250_Wills_S1_001_rmMT_peak_21368    4.86398   4.38956   7.26841
    ##   [21369] A250_Wills_S1_001_rmMT_peak_21369    6.44172   9.95357  13.20674
    ##   [21370] A250_Wills_S1_001_rmMT_peak_21370    4.57143   5.03925   7.97092
    ##   [21371] A250_Wills_S1_001_rmMT_peak_21371    2.92929   4.27084   7.12895
    ##   [21372] A250_Wills_S1_001_rmMT_peak_21372    3.30789   4.80259   7.71486
    ##               Score RelativeSummitPosition               Gene
    ##           <integer>              <integer>        <character>
    ##       [1]        51                    131 ENSCAFG00000030108
    ##       [2]        68                    227 ENSCAFG00000030108
    ##       [3]       162                    187 ENSCAFG00000030108
    ##       [4]        59                    121 ENSCAFG00000030108
    ##       [5]        40                    110 ENSCAFG00000030108
    ##       ...       ...                    ...                ...
    ##   [21368]        43                    117 ENSCAFG00000019642
    ##   [21369]        99                    120 ENSCAFG00000033806
    ##   [21370]        50                    162 ENSCAFG00000037301
    ##   [21371]        42                    119 ENSCAFG00000037301
    ##   [21372]        48                    131 ENSCAFG00000037301
    ##           DistanceToTSS
    ##               <integer>
    ##       [1]        262882
    ##       [2]        254863
    ##       [3]        249688
    ##       [4]        246051
    ##       [5]        240261
    ##       ...           ...
    ##   [21368]         88849
    ##   [21369]          9439
    ##   [21370]         12830
    ##   [21371]         36953
    ##   [21372]         38863
    ##   -------
    ##   seqinfo: 1889 sequences from an unspecified genome; no seqlengths

Coverage Views
--------------

``` r
##### Basic setup to tell comp where the bam file is
bam    <- BamFile("A250_subsample.bam")

##### Read in the bamfile
aln    <- readGAlignments(bam)
aln    <- GRanges(aln)

##### Fucntion that shifts reads based on strand aligned to
shiftbasedonstrand <- function(alnGR, shiftsize) {
    pos <- which(strand(alnGR) == "+")
    neg <- which(strand(alnGR) == "-")
    shifts <- vector(length = length(alnGR))
    shifts[pos] <- shiftsize
    shifts[neg] <- -shiftsize
    shiftedGR <- shift(alnGR, shifts)
    return(shiftedGR)
}
aln <- shiftbasedonstrand(aln, -100)

##### Resize reads to fixed fragment length (200)
aln <- resize(aln, 200)


##### Extract basepair coverage
cov <- coverage(aln)


##### Convert to RPM
rpmfun <- function(covob, libsize) {
  signif(covob/libsize * 10^6, 5)
}
nreads <- length(aln)
rpmfile <- mapply(rpmfun,
                  covob = cov,
                  libsize=nreads)
rpmfile <- as(rpmfile, "SimpleRleList")
rpmfile
```

    ## RleList of length 3268
    ## $`1`
    ## numeric-Rle of length 122678785 with 373612 runs
    ##   Lengths:    1031      52      62      86 ...     133      47     414
    ##   Values :       0 0.52703 0.79055  1.0541 ... 0.52703 0.26352       0
    ## 
    ## $`10`
    ## numeric-Rle of length 69331447 with 231494 runs
    ##   Lengths:   13819     200     114     200 ...     172     200       7
    ##   Values :       0 0.26352       0 0.26352 ...       0 0.26352       0
    ## 
    ## $`11`
    ## numeric-Rle of length 74389097 with 198493 runs
    ##   Lengths:       4       1       7       5 ...     108      65      16
    ##   Values : 0.79055  1.0541  1.3176  1.5811 ... 0.52703 0.26352       0
    ## 
    ## $`12`
    ## numeric-Rle of length 72498081 with 181696 runs
    ##   Lengths:    4603      33       8      25 ...       1       1       1
    ##   Values :       0 0.26352 0.52703 0.79055 ...  1003.2  997.15  977.91
    ## 
    ## $`13`
    ## numeric-Rle of length 63241923 with 170144 runs
    ##   Lengths:    2631     200     137     160 ...     138      39    3086
    ##   Values :       0 0.26352       0 0.26352 ... 0.52703 0.26352       0
    ## 
    ## ...
    ## <3263 more elements>

``` r
## saveRDS(rpmfile, "Test.RPM")
## export.bw(object=rpmfile, con=bw_output)
```

``` r
##### Read in full coverage RDS
A250.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/A250_rpm.RDS")

##### Find the converage at each individual basepair for each gene
Promoters.gr <- promoters(genes.gr, upstream=2000, downstream = 2000)

##### Looking at specific Ranges
Promoters.gr["ENSCAFG00000031236"]
```

    ## GRanges object with 1 range and 0 metadata columns:
    ##                      seqnames               ranges strand
    ##                         <Rle>            <IRanges>  <Rle>
    ##   ENSCAFG00000031236        9 [19845323, 19849322]      +
    ##   -------
    ##   seqinfo: 3268 sequences (1 circular) from an unspecified genome

``` r
##### Get the chromosome name
ch <- as.character(unique(seqnames(Promoters.gr["ENSCAFG00000031236"])))

##### Pull out vector of cooverage at that area
view <- Views(A250.rpm[ch][[1]], ranges(Promoters.gr["ENSCAFG00000031236"]))

##### Convert to numeric non-rle style
cov.v <- as.numeric(view[1][[1]])
if (as.logical(strand(Promoters.gr["ENSCAFG00000031236"]) == "-")) {
  cov <- rev(cov)
}

##### Plot what looks like 
plot(cov.v, type = "l")
```

![](README_files/figure-markdown_github/Views-1.png)

``` r
plot(cov.v,
     main = "Coverage at ENSCAFG00000031236",
     xaxt = "n",
     type = "l",
     col  = "red",
     ylab = "Reads Per Million",
     xlab = "Position")
axis(1, at=c(0, 2000, 4000), labels=c("-2000", "TSS", "2000"))
```

![](README_files/figure-markdown_github/Views-2.png)

``` r
######## Lets plot different samples
##### Make a function
ExtractCov <- function(gr, rpmcov, gene) {
  ch <- as.character(unique(seqnames(gr[gene])))
  view <- Views(rpmcov[ch][[1]], ranges(gr[gene]))
  cov.v <- as.numeric(view[1][[1]])
  if (as.logical(strand(gr[gene]) == "-")) {
    cov.v <- rev(cov.v)
  }
  return(cov.v)
}



##### No let's see how different samples compare
B250.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/B250_rpm.RDS")
C250.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/C250_rpm.RDS")
C50.rpm <- readRDS("~/Desktop/ATACWorkingGroupGitLab/WorkingDirectory/C50_rpm.RDS")


##### Extract Coverage
B250.cov <- ExtractCov(Promoters.gr, B250.rpm, "ENSCAFG00000031236")
C250.cov <- ExtractCov(Promoters.gr, C250.rpm, "ENSCAFG00000031236")
C50.cov  <- ExtractCov(Promoters.gr, C50.rpm, "ENSCAFG00000031236")

##### plot the vector 
plot(cov.v,
     main = "Coverage at ENSCAFG00000031236",
     xaxt = "n",
     type = "l",
     col  = "red",
     ylab = "Reads Per Million",
     xlab = "Position")
##### Plot lines for other samples
lines(B250.cov, col = "blue")
lines(C250.cov, col = "purple")
lines(C50.cov,  col = "green")
##### set the Axis
axis(1, at=c(0, 2000, 4000), labels=c("-2000", "TSS", "2000"))
legend(3000,3, # places a legend at the appropriate place 
       c("A250","B250", "C250", "C50"), # puts text in the legend 
       lty=c(1,1), # gives the legend appropriate symbols (lines)
       lwd=c(2.5,2.5),
       col=c("red","blue", "purple", "green"))
```

![](README_files/figure-markdown_github/Views-3.png)
