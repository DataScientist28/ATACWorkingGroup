# Welcome

Welcome to the github repository for the ATAC-seq Working Group. Here I will post all of the files and data needed for each weeks tutorial. Also, if we wanted, I could post people's presentation slides. Each header will correspong to a specific folder that will contain the needed file(s) for each meeting.

# DownloadingR

Please go to the downloadingR section to download RStudio **BEFORE** the first meeting! Thanks!

# Session 1 - Intro to R and Genomic Ranges
This folder should contain the source code for the tutorial. 

# Session 2 - Advanced R and Genomic Ranges
I made a new folder that will contain the files that we will use in the tutorial. This file is labeled WorkingDirectory and then this directory will be set via `setwd`


Feel free to email me if you have any questions! (achits[at]uw.edu)
