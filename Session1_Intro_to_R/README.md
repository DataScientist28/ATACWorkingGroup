ATAC-seq Working Group - Intro to R (GenomicRanges)
================
Alex Chitsazan
February 3, 2017

-   [Getting Started and Help](#getting-started-and-help)
-   [Convert to GRanges](#convert-to-granges)
-   [Pulling Statistics From GRanges object](#pulling-statistics-from-granges-object)

### Getting Started and Help

``` r
# Load the manual for the command read.table()
help("read.table")
print("1")
```

    ## [1] "1"

``` r
# Check what directory your computer thinks you are working in
getwd()
```

    ## [1] "/Users/willslab/Desktop/ATACWorkingGroup/Session1_Intro_to_R"

``` r
# Set the directory of where the data that you want to download is
setwd("~/Desktop/ATACWorkingGroup/Session1_Intro_to_R/")
```

3

``` r
# Load the MACS results of peak locations and associated metrics (A250)
A250 <- read.table("A250_Wills_S1_001_rmMT_peaks.narrowPeak", 
                   header = FALSE,
                   sep = "\t")
# Showing what raw file looks like
head(A250)
```

    ##   V1    V2    V3                            V4  V5 V6      V7       V8
    ## 1  1  6810  7122 A250_Wills_S1_001_rmMT_peak_1  51  . 3.93258  8.11156
    ## 2  1 14838 15141 A250_Wills_S1_001_rmMT_peak_2  68  . 3.51064  9.96358
    ## 3  1 20004 20316 A250_Wills_S1_001_rmMT_peak_3 162  . 4.80769 19.82099
    ## 4  1 23639 23953 A250_Wills_S1_001_rmMT_peak_4  59  . 3.63881  8.90987
    ## 5  1 29543 29743 A250_Wills_S1_001_rmMT_peak_5  40  . 4.65116  6.92693
    ## 6  1 46274 46564 A250_Wills_S1_001_rmMT_peak_6  70  . 4.58167 10.18222
    ##         V9 V10
    ## 1  5.16883 131
    ## 2  6.89208 227
    ## 3 16.26406 187
    ## 4  5.90736 121
    ## 5  4.08593 110
    ## 6  7.09755 145

``` r
# Add Appropriate Column Names
colnames(A250) <- c("Chrom", "Start", "End", "Name", "Score", "Strand", 
                    "FoldChange", "pValue", "qValue", "RelativeSummitPosition")

# Change Strand from "." to "*" because that's how GRanges likes it
A250$Strand <- "*"
head(A250)
```

    ##   Chrom Start   End                          Name Score Strand FoldChange
    ## 1     1  6810  7122 A250_Wills_S1_001_rmMT_peak_1    51      *    3.93258
    ## 2     1 14838 15141 A250_Wills_S1_001_rmMT_peak_2    68      *    3.51064
    ## 3     1 20004 20316 A250_Wills_S1_001_rmMT_peak_3   162      *    4.80769
    ## 4     1 23639 23953 A250_Wills_S1_001_rmMT_peak_4    59      *    3.63881
    ## 5     1 29543 29743 A250_Wills_S1_001_rmMT_peak_5    40      *    4.65116
    ## 6     1 46274 46564 A250_Wills_S1_001_rmMT_peak_6    70      *    4.58167
    ##     pValue   qValue RelativeSummitPosition
    ## 1  8.11156  5.16883                    131
    ## 2  9.96358  6.89208                    227
    ## 3 19.82099 16.26406                    187
    ## 4  8.90987  5.90736                    121
    ## 5  6.92693  4.08593                    110
    ## 6 10.18222  7.09755                    145

``` r
# look at column one and two
# Acess rows and columns in R by brackets: table_name[Row, Column]
head(A250[,1:2])
```

    ##   Chrom Start
    ## 1     1  6810
    ## 2     1 14838
    ## 3     1 20004
    ## 4     1 23639
    ## 5     1 29543
    ## 6     1 46274

``` r
# Look at row 1 and 2
A250[1:2,]
```

    ##   Chrom Start   End                          Name Score Strand FoldChange
    ## 1     1  6810  7122 A250_Wills_S1_001_rmMT_peak_1    51      *    3.93258
    ## 2     1 14838 15141 A250_Wills_S1_001_rmMT_peak_2    68      *    3.51064
    ##    pValue  qValue RelativeSummitPosition
    ## 1 8.11156 5.16883                    131
    ## 2 9.96358 6.89208                    227

``` r
# Same for B250 
B250 <- read.table("B250_Wills_S2_001_rmMT_peaks.narrowPeak", 
                   header = FALSE,
                   sep = "\t")
colnames(B250) <- c("Chrom", "Start", "End", "Name", "Score", "Strand", 
                    "FoldChange", "pValue", "qValue", "RelativeSummitPosition")
B250$Strand <- "*"
```

### Convert to GRanges

Here is a link to the GenomicRanges [Manual](http://bioconductor.org/packages/release/bioc/html/GenomicRanges.html)

``` r
# Download Library
source("https://bioconductor.org/biocLite.R")
```

    ## Bioconductor version 3.3 (BiocInstaller 1.22.3), ?biocLite for help

    ## A newer version of Bioconductor is available for this version of R,
    ##   ?BiocUpgrade for help

``` r
#RUN THIS biocLite("GenomicRanges")
library(GenomicRanges)

# Convert to GRanges Format 
A250.gr <- with(A250, GRanges(Chrom, 
                              IRanges(Start, End),
                              Strand, 
                              Name, 
                              FoldChange, 
                              qValue, 
                              pValue,
                              Score,
                              RelativeSummitPosition))


B250.gr <- with(B250, GRanges(Chrom, 
                              IRanges(Start, End),
                              Strand, 
                              Name, 
                              FoldChange, 
                              qValue, 
                              pValue,
                              Score,
                              RelativeSummitPosition))

# View output
A250.gr
```

    ## GRanges object with 21372 ranges and 6 metadata columns:
    ##           seqnames                 ranges strand |
    ##              <Rle>              <IRanges>  <Rle> |
    ##       [1]        1         [ 6810,  7122]      * |
    ##       [2]        1         [14838, 15141]      * |
    ##       [3]        1         [20004, 20316]      * |
    ##       [4]        1         [23639, 23953]      * |
    ##       [5]        1         [29543, 29743]      * |
    ##       ...      ...                    ...    ... .
    ##   [21368]        X [123528125, 123528493]      * |
    ##   [21369]        X [123717289, 123717744]      * |
    ##   [21370]        X [123841524, 123841852]      * |
    ##   [21371]        X [123865647, 123865935]      * |
    ##   [21372]        X [123867557, 123867763]      * |
    ##                                        Name FoldChange    qValue    pValue
    ##                                    <factor>  <numeric> <numeric> <numeric>
    ##       [1]     A250_Wills_S1_001_rmMT_peak_1    3.93258   5.16883   8.11156
    ##       [2]     A250_Wills_S1_001_rmMT_peak_2    3.51064   6.89208   9.96358
    ##       [3]     A250_Wills_S1_001_rmMT_peak_3    4.80769  16.26406  19.82099
    ##       [4]     A250_Wills_S1_001_rmMT_peak_4    3.63881   5.90736   8.90987
    ##       [5]     A250_Wills_S1_001_rmMT_peak_5    4.65116   4.08593   6.92693
    ##       ...                               ...        ...       ...       ...
    ##   [21368] A250_Wills_S1_001_rmMT_peak_21368    4.86398   4.38956   7.26841
    ##   [21369] A250_Wills_S1_001_rmMT_peak_21369    6.44172   9.95357  13.20674
    ##   [21370] A250_Wills_S1_001_rmMT_peak_21370    4.57143   5.03925   7.97092
    ##   [21371] A250_Wills_S1_001_rmMT_peak_21371    2.92929   4.27084   7.12895
    ##   [21372] A250_Wills_S1_001_rmMT_peak_21372    3.30789   4.80259   7.71486
    ##               Score RelativeSummitPosition
    ##           <integer>              <integer>
    ##       [1]        51                    131
    ##       [2]        68                    227
    ##       [3]       162                    187
    ##       [4]        59                    121
    ##       [5]        40                    110
    ##       ...       ...                    ...
    ##   [21368]        43                    117
    ##   [21369]        99                    120
    ##   [21370]        50                    162
    ##   [21371]        42                    119
    ##   [21372]        48                    131
    ##   -------
    ##   seqinfo: 1889 sequences from an unspecified genome; no seqlengths

``` r
B250.gr
```

    ## GRanges object with 34166 ranges and 6 metadata columns:
    ##           seqnames                 ranges strand |
    ##              <Rle>              <IRanges>  <Rle> |
    ##       [1]        1         [14841, 15133]      * |
    ##       [2]        1         [19977, 20318]      * |
    ##       [3]        1         [23116, 23894]      * |
    ##       [4]        1         [39392, 39592]      * |
    ##       [5]        1         [46100, 46564]      * |
    ##       ...      ...                    ...    ... .
    ##   [34162]        X [123717229, 123717763]      * |
    ##   [34163]        X [123760252, 123760538]      * |
    ##   [34164]        X [123784484, 123784718]      * |
    ##   [34165]        X [123841452, 123842261]      * |
    ##   [34166]        X [123865647, 123865994]      * |
    ##                                        Name FoldChange    qValue    pValue
    ##                                    <factor>  <numeric> <numeric> <numeric>
    ##       [1]     B250_Wills_S2_001_rmMT_peak_1    2.74390   3.58352   6.16013
    ##       [2]     B250_Wills_S2_001_rmMT_peak_2    3.74204  11.26062  14.35530
    ##       [3]     B250_Wills_S2_001_rmMT_peak_3    4.31965  11.61192  14.72265
    ##       [4]     B250_Wills_S2_001_rmMT_peak_4    2.89017   2.89009   5.36971
    ##       [5]     B250_Wills_S2_001_rmMT_peak_5    5.44118  14.12409  17.33924
    ##       ...                               ...        ...       ...       ...
    ##   [34162] B250_Wills_S2_001_rmMT_peak_34162    9.19811  24.18079  27.76884
    ##   [34163] B250_Wills_S2_001_rmMT_peak_34163    4.39242   4.13844   6.78229
    ##   [34164] B250_Wills_S2_001_rmMT_peak_34164    4.39242   4.13844   6.78229
    ##   [34165] B250_Wills_S2_001_rmMT_peak_34165    5.47945   9.49289  12.50393
    ##   [34166] B250_Wills_S2_001_rmMT_peak_34166    2.51451   2.78575   5.25251
    ##               Score RelativeSummitPosition
    ##           <integer>              <integer>
    ##       [1]        35                    126
    ##       [2]       112                    198
    ##       [3]       116                    302
    ##       [4]        28                     51
    ##       [5]       141                    264
    ##       ...       ...                    ...
    ##   [34162]       241                    307
    ##   [34163]        41                    150
    ##   [34164]        41                    105
    ##   [34165]        94                    179
    ##   [34166]        27                    266
    ##   -------
    ##   seqinfo: 2197 sequences from an unspecified genome; no seqlengths

``` r
# Modularity Example
Peaks2Ranges <- function(PEAKFILE) {
  # Load the MACS results of peak locations and associated metrics
  peaks.table <- read.table(PEAKFILE, 
                           header = FALSE, 
                           sep = "\t")
  
  # Add Appropriate Column Names
  colnames(peaks.table) <- c("Chrom", "Start", "End", "Name", "Score", "Strand", 
                            "FoldChange", "pValue", "qValue", "RelativeSummitPosition")
  # Change Strand from "." to "*" because that's how GRanges likes it
  peaks.table$Strand <- "*"
  
  # Convert to GRanges Format 
  peak.gr <- with(peaks.table, GRanges(Chrom, 
                              IRanges(Start, End),
                              Strand, 
                              Name, 
                              FoldChange, 
                              qValue, 
                              pValue,
                              Score,
                              RelativeSummitPosition))
  return(peak.gr)
}

# Running Function
A250.gr <- Peaks2Ranges("A250_Wills_S1_001_rmMT_peaks.narrowPeak")
B250.gr <- Peaks2Ranges("B250_Wills_S2_001_rmMT_peaks.narrowPeak")
```

### Pulling Statistics From GRanges object

``` r
# Basic things you can do with a GRanges object
head(width(A250.gr))
```

    ## [1] 313 304 313 315 201 291

``` r
# What is the distribution of peak width
summary(width(A250.gr))
```

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   201.0   228.0   302.0   371.6   451.0  3228.0

``` r
# Histogram of peak width
hist(width(A250.gr))
```

![](README_files/figure-markdown_github/Hist-1.png)

``` r
hist(width(A250.gr), 
     xlim   = c(0, 1000),
     breaks = 120,
     col    = "red",
     main   = "Peak Width of A250",
     xlab   = "Peak Width")
```

![](README_files/figure-markdown_github/Hist-2.png)

``` r
hist(width(B250.gr),
     xlim   = c(0,1000),
     breaks = 120,
     col    = "Green4",
     main   = "Peak Width of B250",
     xlab   = "Peak Width")
```

![](README_files/figure-markdown_github/Hist-3.png)

``` r
# Plot them on top of eachother
par(mfrow=c(2,1)) 
hist(width(A250.gr), 
     xlim   = c(0, 1000),
     breaks = 120,
     col    = "red",
     main   = "Peak Width of A250",
     xlab   = "Peak Width")

hist(width(B250.gr),
     xlim   = c(0,1000),
     breaks = 120,
     col    = "Green4",
     main   = "Peak Width of B250",
     xlab   = "Peak Width")
```

![](README_files/figure-markdown_github/histWidth-1.png)

``` r
# Number of Peaks per chromosome
chroms <- table(seqnames(B250.gr))[1:38]

# Sort 
chroms <- chroms[order(as.numeric(names(chroms)))]

# barplot of number of peaks per chromosome
barplot(chroms,
        col = "purple",
        xlab = "Chromosome",
        ylab = "Number of Peaks",
        main = "Number of Peaks Per Chromosome")
```

![](README_files/figure-markdown_github/barplotChroms-1.png)
